package kg.neobis.smarttailor.enums;

public enum Role {
    GUEST,
    USER,
    ADMIN,
    OWNER
}
