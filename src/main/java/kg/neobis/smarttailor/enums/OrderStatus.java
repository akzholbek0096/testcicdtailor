package kg.neobis.smarttailor.enums;

public enum OrderStatus {
    WAITING,
    IN_PROGRESS,
    CHECKING,
    SENDING,
    ARRIVED
}
