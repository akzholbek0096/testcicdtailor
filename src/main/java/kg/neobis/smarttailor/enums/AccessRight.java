package kg.neobis.smarttailor.enums;

public enum AccessRight {
    CREATE_ORDER,
    CREATE_ISSUE_POSITION,
    EDIT_POSITION_ACCESS_RIGHTS,
    ADD_EMPLOYEE,
    EDIT_ORDER_STATUS,
    REMOVE_ORDER,
    REMOVE_EMPLOYEE,
    REMOVE_POSITION

}
